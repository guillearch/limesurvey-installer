# LimeSurvey CE GNU/Linux Installer

This script automates the installation of the **open source survey tool LimeSurvey Community Edition**, along with all the required third party components. At the end you should get LimeSurvey CE running in a LAMP (GNU/Linux, Apache, MySQL and PHP) environment. Some manual tasks will be needed during the process.

The code is licensed under the GNU General Public License v3.0. Feel free to adapt the script for your needs!

## Requirements

These are the GNU/Linux distributions currently supported:

- Ubuntu: 16.04, 18.04.

Note that you will have to run a web installer to complete the install.

Please make sure you have sudo privileges before continuing and you meet the minimum hardware requirements for the desired version, which are detailed in the [official documentation](https://manual.limesurvey.org/Installation_-_LimeSurvey_CE).

## Downloading the installer

Clone the repository with the following command:

```
git clone https://gitlab.com/guillearch/limesurvey-installer.git
```

## Running the script

Move to the script directory:

```
cd limesurvey-installer/
```

Make the script executable:

```
sudo chmod +x install.sh
```

Run the script:

```
sudo ./install.sh
```

Follow the instructions printed in the terminal.

## Parameters

The script accepts the following parameters:

| Parameter | Description                                                                    |
| --------- | ------------------------------------------------------------------------------ |
| -h        | Display help text.                                                             |
| -v        | Set the version to install (lts, unstable or dev). Default (recommended): lts. |
| -db       | Set the database name. Default: limesurvey_db.                                 |
| -u        | Set the database username. Default: limesurvey_user.                           |

## Running the web installer

The script will prompt you to run the web installer in order to complete the installation. To do so, open your browser and type `<your-host>/limesurvey/admin` in the address bar. `<yout-host>` is `localhost`, if you're installing LimeSurvey CE in a local machine; or the IP address if you're doing a remote installation.

Follow the instructions provided by the web installer to finish the process.

## Further configuration

Once the installation is done, you may want to:

- Go to Global Settings > Email Settings and change email method to SMTP.
- Install Let's Encrypt.
- Add a custom domain.
